import psycopg2
import config
import random

def connect():
    try:
        conn = psycopg2.connect(dbname   = config.DB_NAME,
                                user     = config.USER,
                                password = config.PASSWORD,
                                host     = config.HOST,
                                port     = config.PORT)
        print("Соеденение установлено")
        return conn

    except psycopg2.Error as err:
        print("Connection error: {}".format(err))
        return None

def insert_user_data(user_data):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""INSERT INTO public.sick (vk_link, name, surname , midlname , date_born ,
                    pasport_seria, pasport_number, pasport_date, pasport_location, city, info , score, rating)
                    VALUES('{user_data[0]}', '{user_data[1]}', '{user_data[2]}', '{user_data[3]}',
                    '{user_data[4]}', '{user_data[5]}', '{user_data[6]}', '{user_data[7]}', '{user_data[8]}',  '{user_data[9]}' , '{0}', {0}, {0} )""")
    conn.commit()
    conn.close()

def select_user_id(user_id):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT vk_link FROM public.sick WHERE sick.vk_link = '{user_id}'""")
    row = cursor.fetchall()
    conn.commit()
    conn.close()
    if len(row)>0:
        return True
    else:
        return False


def insert_service(user_id, text):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT id FROM public.sick WHERE vk_link = '{user_id}' """)
    row = cursor.fetchall()
    cursor.execute(f"""INSERT INTO public.service (customer_id , service_text) VALUES ('{row[0][0]}', '{text}')""")
    conn.commit()
    conn.close()


def insert_redact_data(text , user_id , key):
    column = {"Имя":"name", "Фамилия":"surname", "Отчество":"midlname", "Дата рождения":"date_born", "Город":"city",
                "Серия":"pasport_seria", "Номер":"pasport_number", "Дата выдачи":"pasport_date", "Место выдачи":"pasport_location"}
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""UPDATE public.sick SET {column[key]} = '{text}' WHERE vk_link = '{user_id}'""")
    conn.commit()
    conn.close()

def select_reserv(user_id):
        conn = connect()
        cursor = conn.cursor()
        cursor.execute(f"""SELECT id FROM public.sick WHERE sick.vk_link = '{user_id}'""")
        row = cursor.fetchall()
        cursor.execute(f"""SELECT * FROM public.service WHERE customer_id = '{row[0][0]}'""")
        row = cursor.fetchall()
        conn.commit()
        conn.close()
        return row

def delete_reserv(row):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""DELETE FROM public.service WHERE service_id = '{row}'""")
    conn.commit()
    conn.close()

def select_user_info():
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT * FROM public.sick """)
    row = cursor.fetchall()
    conn.commit()
    conn.close()
    return row



# Функции исполнителя
def insert_volonter_data(user_data):
        print(user_data)
        conn = connect()
        cursor = conn.cursor()
        cursor.execute(f"""INSERT INTO public.volonter (volonter_vk_link, volonter_name, volonter_surname , volonter_midlname , volonter_date_born ,
                        volonter_pasport_seria, volonter_pasport_number, volonter_pasport_date, volonter_pasport_location, volonter_city, volonter_info , volonter_score, volonter_rating)
                        VALUES('{user_data[0]}', '{user_data[1]}', '{user_data[2]}', '{user_data[3]}',
                        '{user_data[4]}', '{user_data[5]}', '{user_data[6]}', '{user_data[7]}', '{user_data[8]}', '{user_data[9]}', '{0}', '{0}', '{0}' )""")
        conn.commit()
        conn.close()


def insert_volonter_redact_data(text , user_id , key):
    column = {"Имя":"name", "Фамилия":"surname", "Отчество":"midlname", "Дата рождения":"date_born", "Город":"city",
                "Серия":"pasport_seria", "Номер":"pasport_number", "Дата выдачи":"pasport_date", "Место выдачи":"pasport_location"}
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""UPDATE public.volonter SET volonter_{column[key]} = '{text}' WHERE volonter_vk_link = '{user_id}'""")
    conn.commit()
    conn.close()

def select_volonter_id(user_id):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT volonter_vk_link FROM public.volonter WHERE volonter.volonter_vk_link = '{user_id}'""")
    row = cursor.fetchall()
    conn.commit()
    conn.close()
    if len(row)>0:
        return True
    else: return False

def select_all_volonter_id():
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT volonter_vk_link FROM public.volonter """)
    row = cursor.fetchall()
    conn.commit()
    conn.close()
    return row

def not_service(text):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT status FROM public.service WHERE service_text = '{text}'""")
    row = cursor.fetchall()
    conn.commit()
    conn.close()
    return row[0][0]


def insert_id_in_service(user_id, text, user_id_in_event):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT volonter_id FROM public.volonter WHERE volonter.volonter_vk_link = '{user_id}'""")
    row_from_volonter = cursor.fetchall()
    cursor.execute(f"""SELECT service_id FROM public.service WHERE service_text = '{text}'""")
    id  = cursor.fetchall()
    cursor.execute(f"""UPDATE public.service SET status = True , executor_id = {row_from_volonter[0][0]} WHERE service_id = {id[0][0]} """)
    conn.commit()
    conn.close()

def insert_in_complaint(user_id, row , text):
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""INSERT INTO public.complaint (cust , exe , comp_text) VALUES('{user_id}', '{row[3]}', '{text}')""")
    conn.commit()
    conn.close()


def select_top():
    conn = connect()
    cursor = conn.cursor()
    cursor.execute(f"""SELECT volonter_vk_link, volonter_name,volonter_surname,volonter_rating FROM public.volonter""")
    row_from_volonter = cursor.fetchall()
    conn.commit()
    conn.close()

    return row_from_volonter
