from vk_api.longpoll import VkLongPoll, VkEventType
import vk_api
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
import config
import random
import db
from threading import Thread
vk_session = vk_api.VkApi(token = config.TOKEN)
session_api = vk_session.get_api()
longpoll = VkLongPoll(vk_session)

index = 0
global mess
mess = [
    "Введите Вашу фамилию",
    "Введите Ваше отчество",
    "Введите Вашу дату рождения в формате ДД.ММ.ГГГГ",
    "Введите серию паспорта",
    "Введите номер паспорта",
    "Введите дату выдачи паспорта в формате ДД.ММ.ГГГГ",
    "Введите кем выдан паспорт",
    "Ваш город"
]




def get_random():
    return random.randint(0,10000000000)

def menu():
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Мне нужна помощь" and db.select_user_id(event.user_id):
                user_pallium_menu(event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Мне нужна помощь"  and not db.select_user_id(event.user_id):
                user_data = list()
                user_data.append(event.user_id)
                session_api.messages.send(message = "Введите имя:", user_id = event.user_id , random_id = get_random())
                input_user_date(event.user_id, mess , 1, user_data)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Хочу помогать" and db.select_volonter_id(event.user_id):
                menu_volonter(event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Хочу помогать"  and not db.select_user_id(event.user_id):
                user_data = list()
                user_data.append(event.user_id)
                session_api.messages.send(message = "Введите имя:", user_id = event.user_id , random_id = get_random())
                input_user_date(event.user_id, mess , 2, user_data)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Меню " :
                menu()
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Топ волонтеров":
                top_volonter(event.user_id)

def main():

    keyboard = VkKeyboard(one_time = True)
    keyboard.add_button("Мне нужна помощь", color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Хочу помогать", color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Хочу стать спонсором", color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Топ волонтеров")
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW  and (event.text == "Начать" or event.text == "Меню"):
                session_api.messages.send(message = "Выберете действие:", user_id = event.user_id , random_id = get_random(), keyboard = keyboard.get_keyboard())
                menu()










def input_user_date(user_id, mess, flag , user_data):
    global index
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
                user_data.append(event.text)
                if index < (len(mess)):
                    session_api.messages.send(message = mess[index], user_id = user_id, random_id = get_random())
                    print(user_data)
                    print(index)
                    index = index + 1
                else:
                    print("Зашло")
                    print(user_data)
                    if flag == 1:
                        db.insert_user_data(user_data)
                        user_pallium_menu(event.user_id)
                    if flag == 2:
                        db.insert_volonter_data(user_data)
                        menu_volonter(event.user_id)

                input_user_date(user_id, mess, flag, user_data)


def user_pallium_menu(user_id):
    keyboard = VkKeyboard()
    keyboard.add_button("Найти помощь")
    keyboard.add_line()
    keyboard.add_button("Оставить отзыв о исполнителе")
    keyboard.add_line()
    keyboard.add_button("Редактировать профиль")
    keyboard.add_line()
    keyboard.add_button("Отменить заявку на помощь")

    session_api.messages.send(message = "Добро пожаловать в ваше меню", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Найти помощь":
                session_api.messages.send(message = "Опишите ситуацию:", user_id = user_id, random_id = get_random())
                problem(event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Отменить заявку на помощь":
                row = db.select_reserv(user_id)
                view_reserv(row, event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Редактировать профиль":
                profile_redact(event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Оставить отзыв о исполнителе":
                session_api.messages.send(message = "Введите количество баллов от 0 до 10:", user_id = user_id, random_id = get_random())
                row = db.select_user_info()
                view_volonter(event.user_id , row)



def top_volonter(user_id):
    row = db.select_top()
    str = "Топ волонтеров:\n"
    for i in range(0,len(row)):
        str += f"{i+1}. {row[len(row)-i-1][2]} {row[len(row)-i-1][1]}\n Рейтинг: {row[len(row)-i-1][3]} \n vk.com/id{row[len(row)-i-1][0]}\n\n"
    session_api.messages.send(message = str, user_id = user_id, random_id = get_random())

def view_volonter(user_id, row):

    global index_donos

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Оставить отзыв', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)

    ln   = len(row)
    slise_user(row, user_id)

    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Далее':
                index_donos = index_reserv + 1
                if index_donos == ln or ln == 1 :
                    index_donos = 0
                slise_volonter(row , event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Назад':
                index_donos = index_donos - 1
                if index_donos == -1 or ln == 1:
                    index_donos = ln - 1
                slise_volonter(row , event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Меню':
                user_pallium_menu(event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Оставить отзыв':
                '''Оставить отзыв'''
                session_api.messages.send(message = "Введите отзыв", user_id = event.user_id, random_id = get_random())
                donos(event.user_id, row[index_donos])
                index_donos = index_donos - 1
                ln    = ln - 1
                if index_donos == -1 or ln == 1:
                    index_donos = ln - 1
                elif index_donos == ln or ln == 1 :
                    index_donos = 0
                row  = db.select_user_info(user_id)
                print_cancel(event.user_id)


def slise_volonter(row, user_id):

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Оставить отзыв', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)
    global index_reserv

    session_api.messages.send(message = f"'{row[index_donos][0]}' '{row[index_donos][1]}' '{row[index_donos][2]}' vk.com\\\\id'{row[index_donos][3]}'", user_id = user_id, random_id = get_random(),
    keyboard = slise_keyboard.get_keyboard())

def score(user_id , row):
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me:
                db.score_plus(user_id, score)
                session_api.messages.send(message = "Напишите отзыв о проделанной работе", user_id = event.user_id, random_id = get_random())
                menu_volonter(event.user_id)

def problem(user_id):
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
                db.insert_service(user_id , event.text)
                spam(event.user_id, event.text)
                user_pallium_menu(user_id)

def spam(user_id, text):
    keyboard = VkKeyboard(one_time = True)
    keyboard.add_button('Принять', color=VkKeyboardColor.POSITIVE)
    keyboard.add_button('Отклонить', color = VkKeyboardColor.NEGATIVE)
    row = db.select_all_volonter_id()
    for i in range( 0, len(row)):
        session_api.messages.send(message = f"Привет, человеку требуется твоя помощь! \n '{text}' \n Вот ссылка vk.com/id{user_id}", user_id = row[i][0],
        random_id = get_random(), keyboard = keyboard.get_keyboard())
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Принять':
                if db.not_service(text) == False:
                    db.insert_id_in_service(user_id, text, event.user_id)
                    session_api.messages.send(message = f"Принято " , user_id = event.user_id,
                    random_id = get_random(), keyboard = keyboard.get_keyboard())
                    menu_volonter(event.user_id)
                else:
                    menu_volonter(event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Отклонить':
                menu_volonter(event.user_id)


def profile_redact(user_id):
    keyboard = VkKeyboard(one_time = True)
    keyboard.add_button("Имя" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Фамилия" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Отчество" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Дата рождения" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Город" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Серия" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Номер" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Дата выдачи" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Место выдачи" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("В меню" , color=VkKeyboardColor.NEGATIVE)
    session_api.messages.send(message = "Выберете пункт:", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text =="В меню":
                user_pallium_menu(event.user_id)
            elif  event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text :
                session_api.messages.send(message = "Введи данные:", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())
                redact_data(event.user_id , event.text)
                session_api.messages.send(message = "Данные изменены", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())



global index_reserv
index_reserv = 0
def slise_reserv(row, user_id):

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Отмена', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)
    global index_reserv
    status = 'Не выполняется' if row[index_reserv][2] == 'null' else "Выполняется"
    session_api.messages.send(message = f"Заказ \n Статус: {status}\n Содержание {row[index_reserv][3]} ", user_id = user_id, random_id = get_random(),
    keyboard = slise_keyboard.get_keyboard())


def redact_data(user_id, key):
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
                db.insert_redact_data(event.text, event.user_id , key)
                break
        break

def view_reserv(row, user_id):

    global index_reserv

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Отмена', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)

    ln   = len(row)
    slise_reserv(row, user_id)

    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Далее':

                index_reserv = index_reserv + 1

                if index_reserv == ln or ln == 1 :

                    index_reserv = 0

                slise_reserv(row , event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Назад':

                index_reserv = index_reserv - 1

                if index_reserv == -1 or ln == 1:

                    index_reserv = ln - 1

                slise_reserv(row , event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Меню':

                user_pallium_menu(event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Отмена':

                db.delete_reserv(row[index_reserv][0])

                index_reserv = index_reserv - 1
                ln    = ln - 1

                if index_reserv == -1 or ln == 1:

                    index_reserv = ln - 1

                elif index_reserv == ln or ln == 1 :

                    index_reserv = 0

                row  = db.select_reserv(user_id)
                print_cancel(event.user_id)



def print_cancel(user_id):

    session_api.messages.send(message = "Заказ отменен",
                              peer_id = user_id,
                              random_id = get_random())

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
#Ветка исполнителя
def menu_volonter(user_id):
    keyboard = VkKeyboard()
    keyboard.add_button("Редактировать личные данные",  color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Магазин",  color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Оставить отзыв",  color=VkKeyboardColor.POSITIVE)
    session_api.messages.send(message = "Добро пожаловать в меню", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Редактировать личные данные':
                profile_volonter_redact(event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Магазин':
                pass
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Оставить отзыв':
                row = db.select_user_info()
                view_user(row, event.user_id)
global index_donos
index_donos = 0

def view_user(row, user_id):

    global index_donos

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Оставить отзыв', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)

    ln   = len(row)
    slise_user(row, user_id)

    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Далее':
                index_donos = index_reserv + 1
                if index_donos == ln or ln == 1 :
                    index_donos = 0
                slise_user(row , event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Назад':
                index_donos = index_donos - 1
                if index_donos == -1 or ln == 1:
                    index_donos = ln - 1
                slise_user(row , event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Меню':
                menu_volonter(event.user_id)
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Оставить отзыв':
                '''Оставить отзыв'''
                session_api.messages.send(message = "Введите отзыв", user_id = event.user_id, random_id = get_random())
                donos(event.user_id, row[index_donos])
                index_donos = index_donos - 1
                ln    = ln - 1
                if index_donos == -1 or ln == 1:
                    index_donos = ln - 1
                elif index_donos == ln or ln == 1 :
                    index_donos = 0
                row  = db.select_user_info(user_id)
                print_cancel(event.user_id)


def donos(user_id , row):
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me:
                db.insert_in_complaint(event.user_id , row , event.text)
                session_api.messages.send(message = "Спасибо за Ваш отзыв ", user_id = event.user_id, random_id = get_random())

                menu_volonter(event.user_id)

def slise_user(row, user_id):

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Оставить отзыв', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)
    global index_reserv

    session_api.messages.send(message = f"'{row[index_donos][0]}' '{row[index_donos][1]}' '{row[index_donos][2]}' vk.com/id'{row[index_donos][3]}'", user_id = user_id, random_id = get_random(),
    keyboard = slise_keyboard.get_keyboard())



def profile_volonter_redact(user_id):
    keyboard = VkKeyboard(one_time = True)
    keyboard.add_button("Имя" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Фамилия" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Отчество" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Дата рождения" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Город" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Серия" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Номер" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Дата выдачи" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Место выдачи" , color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("В меню" , color=VkKeyboardColor.NEGATIVE)
    session_api.messages.send(message = "Выберете пункт:", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text =="В меню":
                menu_volonter(event.user_id)
            elif  event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text :
                session_api.messages.send(message = "Введи данные:", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())
                redact_volonter_data(event.user_id , event.text)
                session_api.messages.send(message = "Данные изменены", user_id = user_id, random_id = get_random(), keyboard = keyboard.get_keyboard())

def redact_volonter_data(user_id, key):
    while True:
        for event in longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
                db.insert_volonter_redact_data(event.text, event.user_id , key)
                break
        break


main()
